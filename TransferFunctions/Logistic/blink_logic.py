# Imported python transfer function that transforms the camera input into a DC current source fed to LGN neurons
import numpy, sensor_msgs.msg
from cv_bridge import CvBridge
@nrp.MapVariable('integrateDuration', scope=nrp.GLOBAL, initial_value=0.1  )
@nrp.MapVariable('isBlinking',        scope=nrp.GLOBAL, initial_value=False)
@nrp.MapVariable('isIntegrating',     scope=nrp.GLOBAL, initial_value=False)
@nrp.MapVariable('isSaccading',       scope=nrp.GLOBAL, initial_value=False)
@nrp.MapVariable('isSegmenting',      scope=nrp.GLOBAL, initial_value=False)
@nrp.Robot2Neuron()
def blink_logic(t, integrateDuration, isBlinking, isIntegrating, isSaccading, isSegmenting):
    
    # Loop timing (small delay, then integration of the  while resetting segmentation, then saccade, then segmentation)
    loopDuration       = 2.0  # units: seconds, duration of a full blink, saccade, segmentation loop
    blinkDuration      = 0.2  # units: seconds, how much time a blink lasts
    integrateDuration  = integrateDuration.value  # units: seconds, how much time saliency is integrated before saccading
    saccadeDelay       = 0.3  # units: seconds, how much before doing a saccade
    segmentationStart  = 0.9  # units: seconds, how much time before segmentation signals are sent (biased value because of de-synchronization)

    isBlinking.value    = False
    isIntegrating.value = False
    isSaccading.value   = False
    isSegmenting.value  = False

    loopCount    = int(t/loopDuration)
    lastLoopTime = loopCount*loopDuration
    timeInLoop   = t-lastLoopTime

    # Choose if this is a normal or a blinking time-step
    if t > 2.0 and loopDuration-blinkDuration <= timeInLoop-0.1 < loopDuration:
        isBlinking.value    = True
        
    # Choose if this is an integrating time-step
    if t > 2.0 and saccadeDelay-integrateDuration <= timeInLoop < saccadeDelay:
        isIntegrating.value = True

    # Right after the integration period comes the saccade
    if t > 2.0 and saccadeDelay <= timeInLoop < saccadeDelay + 0.02:
        isSaccading.value   = True

    # Choose whether this is a normal or a segmentation time-step
    if t > 2.0 and segmentationStart <= timeInLoop < segmentationStart + 0.02 :
        isSegmenting.value  = True
