# Imported Python Transfer Function
@nrp.MapVariable("initialized", initial_value=False)
@nrp.MapVariable("controller_version", initial_value=None, scope=nrp.GLOBAL) # controller variable that holds the control states of version eye movements
@nrp.MapVariable("controller_tilt", initial_value=None, scope=nrp.GLOBAL) # controller variable that holds the control states of tilt eye movements
@nrp.Robot2Neuron()
def init_tf(t, initialized, controller_version, controller_tilt):

    if not initialized.value:
        import sys
        sys.path.insert(0, '.')

        from icub_saccadecontrol import icub_saccadecontrol
        controller_version.value = icub_saccadecontrol()  # control instance for version movements
        controller_tilt.value    = icub_saccadecontrol()  # control instance for tilt movements
        initialized.value = True
