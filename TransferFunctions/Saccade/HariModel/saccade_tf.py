# Imported python transfer function that runs the saccade model in concomitence with the saliency model
import numpy
@nrp.MapRobotSubscriber('joints',            Topic("/robot/joints",          sensor_msgs.msg.JointState))  # this gives us access to the joints of the robot (to get their current values)
@nrp.MapRobotSubscriber('saliencyInput',     Topic("/saliency_map",          sensor_msgs.msg.Image))       # this will make this transfer function able to use the saliency model's
@nrp.MapRobotPublisher( 'eyeTiltVel',        Topic("/robot/eye_tilt/vel",    std_msgs.msg.Float64))        # this variable holds the version velocity commands of each control iteration
@nrp.MapRobotPublisher( 'eyeVersVel',        Topic("/robot/eye_version/vel", std_msgs.msg.Float64))        # this variable holds the tilt velocity commands of each control iteration
@nrp.MapVariable(       'mostSalientPlace',  initial_value=numpy.asfarray([0,0]), scope=nrp.GLOBAL)        # this is used to update / keep track of any value throughout the TF's loops
@nrp.MapVariable(       'integrateDuration', initial_value=0.1,                   scope=nrp.GLOBAL)        # how much time saliency is integrated before saccading
@nrp.MapVariable(       'isIntegrating',     initial_value=False,                 scope=nrp.GLOBAL)
@nrp.MapVariable(       'isSaccading',       initial_value=False,                 scope=nrp.GLOBAL)
@nrp.MapVariable(       'controllerVersion',                                      scope=nrp.GLOBAL)        # variable initialized in the init_tf
@nrp.MapVariable(       'controllerTilt',                                         scope=nrp.GLOBAL)        # variable initialized in the init_tf
@nrp.Neuron2Robot()
def generate_saccades(t, saliencyInput, joints, eyeTiltVel, eyeVersVel, mostSalientPlace, integrateDuration, isIntegrating, isSaccading, controllerVersion, controllerTilt):

    # Here, as an example, I make the eyes of the robot slowly move to the saliency peak. You can use this as an example for what to put in your model!!!
    if saliencyInput.value is not None:

        # Imports
        import numpy
        from cv_bridge import CvBridge
        from scipy import misc

        # Parameters
        pixToRad          = 0.006                    # ratio determined through trial and error
        pixToDeg          = pixToRad*180.0/numpy.pi  # same ratio for degrees (useful here)
        integrateDuration = integrateDuration.value  # units: seconds, for how much time the maximum of the saliency if computed

        # If the saccade time arrives, take a few steps to integrate the maximum of the saliency map
        nIntegrationSteps = int(integrateDuration/0.02)  # to do the mean of the saliency max position
        if isIntegrating.value:

            # Collect the saliency output ; "0.7" is to resize the saliency output (168*224) to the camera image (240*320)
            saliencyArray = misc.imresize(numpy.mean(CvBridge().imgmsg_to_cv2(saliencyInput.value, 'rgb8'), axis=2), 1.0/0.7)

            # Compute the saliency max location, in terms of pixel coordinates ; sum over time-steps since last saccade
            rawMax                 = numpy.unravel_index(numpy.argmax(saliencyArray), numpy.shape(saliencyArray))
            mostSalientPlace.value = mostSalientPlace.value + numpy.asfarray([rawMax[0]-240/2, rawMax[1]-320/2])  # (0,0 is the center of the image and gaze)

        # If the time has come the eyes move to the target
        if joints.value is not None and controller.value is not None and isSaccading.value:

            # Collect the current eyes angles
            currEyeTilt = joints.value.position[joints.value.name.index('eye_tilt'   )]      # get the current eye tilt
            currEyeVers = joints.value.position[joints.value.name.index('eye_version')]      # get the current eye version

            # Transform the 2D pixel vector into an eye tilt and an eye version
            targetTiltShift = -mostSalientPlace.value[0]*pixToDeg/nIntegrationSteps          # direction and amplitude of target row
            targetVersShift = -mostSalientPlace.value[1]*pixToDeg/nIntegrationSteps          # direction and amplitude of target column

            # Use the current values of the eyes angles and update them if not too extreme
            if currEyeTilt+targetTiltShift < 0.5 and currEyeVers+targetVersShift < 0.7:

                BG_out_version = controller_version.value.controlIter(Target_version, t)     # call the controlIter method of the icub_saccadecontrol custom module for saccadic command at each time iter 
                BG_out_tilt    = controller_tilt.value.controlIter(   Target_tilt,    t)     # call the controlIter method of the icub_saccadecontrol custom module for saccadic command at each time iter

                eyeVersVel.send_message(std_msgs.msg.Float64(BG_out_version))                   # send velocity inputs
                eyeTiltVel.send_message(std_msgs.msg.Float64(BG_out_tilt   ))                   # send velocity inputs

			# Reset the saliency maximum computation
            mostSalientPlace.value = numpy.asfarray([0,0])
