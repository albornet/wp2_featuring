# Imported python transfer function that uses the saliency model output to generate saccades
import numpy
@nrp.MapRobotSubscriber('joints',            Topic("/robot/joints",          sensor_msgs.msg.JointState))
@nrp.MapRobotSubscriber('saliency_map',      Topic("/robot/saliency_map",    sensor_msgs.msg.Image     ))
@nrp.MapRobotPublisher( 'eyeTilt',           Topic("/robot/eye_tilt/pos",    std_msgs.msg.Float64      ))
@nrp.MapRobotPublisher( 'eyeVers',           Topic("/robot/eye_version/pos", std_msgs.msg.Float64      ))
@nrp.MapVariable(       'mostSalientPlace',  initial_value=numpy.asfarray([0,0]), scope=nrp.GLOBAL      )
@nrp.MapVariable(       'integrateDuration', initial_value=0.1,                   scope=nrp.GLOBAL      )
@nrp.MapVariable(       'isIntegrating',     initial_value=False,                 scope=nrp.GLOBAL      )
@nrp.MapVariable(       'isSaccading',       initial_value=False,                 scope=nrp.GLOBAL      )
@nrp.Neuron2Robot()
def generate_saccades(t, saliency_map, joints, eyeTilt, eyeVers, mostSalientPlace, integrateDuration, isIntegrating, isSaccading):

    # Do something only if the saliency map is already active
    if saliency_map.value is not None:

        # Imports
        import numpy
        from cv_bridge import CvBridge

        # Parameters
        nRows             = saliency_map.value.height
        nCols             = saliency_map.value.width
        radPerPix         = 0.006/nCols*320.0        # determined through trial and error
        integrateDuration = integrateDuration.value  # units: seconds

        # If the saccade time arrives, take a few steps to integrate the maximum of the saliency map
        nIntegrationSteps = int(integrateDuration/0.02)  # to do the mean of the saliency max position
        if isIntegrating.value:

            # Collect the saliency output
            saliencyArray = numpy.mean(CvBridge().imgmsg_to_cv2(saliency_map.value, 'rgb8'), axis=2)
            (nRows,nCols) = numpy.shape(saliencyArray)

            # Compute the saliency max location, in terms of pixel coordinates ; sum over time-steps since last saccade
            rawMax                 = numpy.unravel_index(numpy.argmax(saliencyArray), (nRows,nCols))
            mostSalientPlace.value = mostSalientPlace.value + numpy.asfarray([rawMax[0]-nRows/2, rawMax[1]-nCols/2])

        # If the time has come the eyes move to the target
        if joints.value is not None and isSaccading.value:

            # Collect the current eyes angles
            currEyeTilt = joints.value.position[joints.value.name.index('eye_tilt')]
            currEyeVers = joints.value.position[joints.value.name.index('eye_version')]

            # Transform the 2D pixel vector into an eye tilt and an eye version
            targetTiltShift = -mostSalientPlace.value[0]*radPerPix/nIntegrationSteps
            targetVersShift = -mostSalientPlace.value[1]*radPerPix/nIntegrationSteps

            # Use the current values of the eyes angles and update them if not too extreme
            if currEyeTilt+targetTiltShift < 0.5 and currEyeVers+targetVersShift < 0.7:
                eyeTilt.send_message(std_msgs.msg.Float64(currEyeTilt+targetTiltShift))
                eyeVers.send_message(std_msgs.msg.Float64(currEyeVers+targetVersShift))

            # Reset the saliency maximum computation
            mostSalientPlace.value = numpy.asfarray([0,0])
