import sensor_msgs.msg
@nrp.MapRobotSubscriber("camera",       Topic("/icub_model/left_eye_camera/image_raw", sensor_msgs.msg.Image))
@nrp.MapRobotPublisher( "saliency_map", Topic("/robot/saliency_map",                   sensor_msgs.msg.Image))
@nrp.MapVariable(       "tf_session",   initial_value=None)
@nrp.MapVariable(       "tf_network",   initial_value=None)
@nrp.MapVariable(       "modelPath",    initial_value=None)
@nrp.MapVariable(       "model",        initial_value=None)
@nrp.Robot2Neuron()
def image_to_saliency(t, camera, saliency_map, tf_session, tf_network, modelPath, model):

    # Make tensorflow available from home directory installation
    import numpy as np, site, os, cv2
    site.addsitedir(os.path.expanduser('~/.opt/tensorflow_venv/lib/python2.7/site-packages'))
    import tensorflow as tf
    from cv_bridge import CvBridge
    newModelPath = os.environ['HBP' ]+'/GazeboRosPackages/src/embodied_attention/model/model.ckpt'
    # newModelPath = os.environ['HOME']+'/Documents/DNN/saliencyCrowding/DeepGaze/DeepGazeII.ckpt'
    # newModelPath = os.environ['HOME']+'/Documents/DNN/saliencyCrowding/DeepGaze/ICF.ckpt'

    # Import the saliency model
    if modelPath.value != newModelPath:

        # Build the tensorflow session and restore the model from path
        modelPath.value  = newModelPath
        tf_session.value = tf.Session()
        tf_network.value = tf.train.import_meta_graph(modelPath.value+'.meta')
        tf_network.value.restore(tf_session.value, modelPath.value)

        # Set the model information (input and output indexes) according its name
        if 'embodied_attention' in modelPath.value:
            graph       = tf.get_default_graph()
            model.value = {
                'name' :  'Kroner',
                'in'   : [[graph.get_tensor_by_name(   'Placeholder_1:0' ),[]]],
                'out'  :   graph.get_operation_by_name('conv2d_8/BiasAdd').outputs[0]}
        elif 'DeepGazeII' in modelPath.value:
            model.value = {
                'name' :  'DeepGazeII',
                'in'   : [[tf.get_collection('input_tensor')[0],[]], [tf.get_collection('centerbias_tensor')[0],[]]],
                'out'  :   tf.get_collection('log_density')[0],
                'scale':   5000}
        elif 'ICF' in modelPath.value:
            model.value = {
                'name' :  'ICF',
                'in'   : [[tf.get_collection('input_tensor')[0],[]], [tf.get_collection('centerbias_tensor')[0],[]]],
                'out'  :   tf.get_collection('log_density')[0],
                'scale':   50000}

    # Use the saliency model
    else:

        # Collect the input image and what has to be fed to the network
        image     = CvBridge().imgmsg_to_cv2(camera.value,'rgb8')
        feed_list = model.value['in']

        # Collect the saliency model's output for Alexander Kroner's network
        if model.value['name'] == 'Kroner':
            feed_list[0][1] = np.expand_dims(np.transpose(image,(2,0,1)),axis=0)
            saliency = tf_session.value.run(model.value['out'],feed_dict=dict(feed_list))[0,0,:,:]
            saliency = 255*(saliency-np.min(saliency))

        # Collect the saliency model's output for DeepGazeII or ICF
        if model.value['name'] in ['DeepGazeII', 'ICF']:
            feed_list[0][1] = np.expand_dims(image,axis=0)
            feed_list[1][1] = np.zeros((1,image.shape[0],image.shape[1],1))
            saliency = tf_session.value.run(model.value['out'],feed_dict=dict(feed_list))[0,:,:,0]
            saliency = 255*np.exp(saliency)*model.value['scale']
            saliency[saliency>255] = 255

        # Publish the output as an image
        saliency  = np.dstack((saliency,saliency,saliency))
        msg_frame = CvBridge().cv2_to_imgmsg(saliency.astype(np.uint8),'rgb8')
        saliency_map.send_message(msg_frame)
