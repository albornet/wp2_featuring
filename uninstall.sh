#!/bin/bash
MODELS=$HBP/Models
EXPERIMENTS=$HBP/Experiments
echo
echo ---------------------------------------------
echo Uninstalling Conjoint WP2 Experiment from NRP
echo ---------------------------------------------
echo

rm -f ${MODELS}/brain_model/visual_segmentation.py
rm -rf ${MODELS}/crowding_virtuallab
bash ${MODELS}/create-symlinks.sh > /dev/null
rm -rf ${EXPERIMENTS}/wp2_featuring
echo
echo "Uninstalling: DONE"