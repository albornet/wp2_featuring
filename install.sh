#!/bin/bash

function log() {
    echo "[WP2 Experiment] $*"
}

function download_saliency () {
    if [ $1 ]; then
        curl -k -o model.ckpt.meta "https://neurorobotics-files.net/owncloud/index.php/s/hdjl7TjzSUqF1Ww/download"
        curl -k -o model.ckpt.index "https://neurorobotics-files.net/owncloud/index.php/s/DCPB80foqkteuC4/download"
        curl -k -o model.ckpt.data-00000-of-00001 "https://neurorobotics-files.net/owncloud/index.php/s/bkpmmvrVkeELapr/download"
        echo "gpu" > config
    else
        curl -k -o model.ckpt.meta "https://neurorobotics-files.net/owncloud/index.php/s/TNpWFSX8xLvfbYD/download"
        curl -k -o model.ckpt.index "https://neurorobotics-files.net/owncloud/index.php/s/sDCFUGTrzJyhDA5/download"
        curl -k -o model.ckpt.data-00000-of-00001 "https://neurorobotics-files.net/owncloud/index.php/s/Scti429S7D11tMv/download"
        echo "cpu" > config
    fi
    curl -k -o DeepGazeII.ckpt.meta "https://neurorobotics-files.net/owncloud/index.php/s/rFJLBrUhiiixQ56/download"
    curl -k -o DeepGazeII.ckpt.index "https://neurorobotics-files.net/owncloud/index.php/s/edlz5EtXfffCPGR/download"
    curl -k -o DeepGazeII.ckpt.data-00000-of-00001 "https://neurorobotics-files.net/owncloud/index.php/s/SY9rfX3ZhV1duZh/download"
    curl -k -o ICF.ckpt.meta "https://neurorobotics-files.net/owncloud/index.php/s/JjPN4GVFg5QXzyn/download"
    curl -k -o ICF.ckpt.index "https://neurorobotics-files.net/owncloud/index.php/s/judIcAkMxtUqzrb/download"
    curl -k -o ICF.ckpt.data-00000-of-00001 "https://neurorobotics-files.net/owncloud/index.php/s/0sMPZaiIoaUTsU9/download"
}


printf "\033[1;33mWould you like to install the GPU version of tensorflow? (Y/n)\033[0m\n"
read tf_gpu
if [ "$tf_gpu" == "Y" -o "$tf_gpu" == "y" ]
then
  echo "The gpu versions of models will be downloaded"
  gpu=1
else
  echo "The cpu versions of models will be downloaded"
  unset gpu
fi


MODELS=$HBP/Models
EXPERIMENTS=$HBP/Experiments
echo
echo -----------------------------------------
echo Installing Conjoint WP2 Experiment in NRP
echo -----------------------------------------
echo
echo "Checking your NRP installation"
echo
echo "\$HBP: ${HBP}"
echo "\$HBP/Models: ${MODELS}"
echo "\$HBP/Experiments: ${EXPERIMENTS}"
echo "\$NRP_VIRTUAL_ENV: ${NRP_VIRTUAL_ENV}"
echo

for dir in ${HBP} ${MODELS} ${EXPERIMENTS} ${NRP_VIRTUAL_ENV}; do
  if [[ ! -d ${dir} ]]; then
    log "The folder ${dir} doesn't exist."
    nrp_folder_not_found=1
  fi
done

nrp_info_link="https://bitbucket.org/hbpneurorobotics/neurorobotics-platform/src/master/"
if [[ ${nrp_folder_not_found} == "1" ]]; then
  log "Please check your NRP installation."
  log "All info available at ${nrp_info_link}."
  exit 1
fi

log "OK: Models, Experiments and platform_venv have been found."
echo
log "Copying models and experiment files"
echo --------------------------------------------

echo
log "Copying models into ${MODELS}"
cp ExpModels/visual_segmentation.py ${MODELS}/brain_model
cp -R ExpModels/crowding_virtuallab ${MODELS}

echo
log "Executing \$HBP/Models/create-symlinks.sh"
bash ${MODELS}/create-symlinks.sh # Create symlinks in ~/.gazebo/models and $HBP/gzweb/http/client/assets

# Copy the content of this experiment folder into $HBP/Experiments/wp2_featuring
excluded_files=(
  ExpModels
  README.md
  install.sh
  uninstall.sh
  .git
)
for f in ${excluded_files[@]}; do
  excluded_paths="${excluded_paths} --exclude=${f}"
done
target_folder=${EXPERIMENTS}/wp2_featuring
source_folder=`dirname $0`
echo
log "Copying files to ${target_folder}"
rsync -av ${excluded_paths} ${source_folder} ${target_folder}

echo
echo
log "Installing dependencies"
echo ----------------------------------------

if [[ $# -ne 0 && $1 == "gpu" ]]; then
  log "Going to download gpu version of the saliency model"
  gpu=1
else
  log "Going to download cpu versions of the saliency model"
  unset gpu
fi

cd $HBP/GazeboRosPackages/src

log "Installing embodied_attention in $HBP/GazeboRosPackages/src"
echo  
if cd embodied_attention; then
  git pull origin master
  cd ..
else
  git clone https://github.com/HBPNeurorobotics/embodied_attention.git
fi

cd embodied_attention/attention
python setup.py install --user
echo
log "Downloading the saliency model"
echo  
cd ..
mkdir -p model
cd model
current_setup=`cat config`
if [ -z $current_setup ]; then
    download_saliency $gpu
elif [ $current_setup  == "gpu" ] && [ $gpu ]; then
    log "GPU weights already present"
elif [ $current_setup == "cpu" ] && [ -z $gpu ]; then
    log "CPU weights already present"
else
    download_saliency $gpu
fi
echo 
log "Building GazeboRosPackages"
echo
cd $HBP/GazeboRosPackages/
catkin_make || { log GAZEBO_ROS_PACKAGES BUILD ERROR; exit 1; }

log "Installing tensorflow in ${HOME}/.opt/tensorflow_venv"
echo
cd ${HOME}/.opt  
virtualenv --system-site-packages tensorflow_venv
source tensorflow_venv/bin/activate
if [ $gpu ]; then
  pip install tensorflow-gpu==1.6.0 || { log TENSORFLOW-GPU: INSTALL ERROR; exit 1; }
else
  pip install tensorflow==1.6.0 || { log TENSORFLOW-CPU: INSTALL ERROR; exit 1; }
fi
# Downgrade protobuf from 3.6.0 to 3.5.0.post1 as the former causes an issue on runtime
pip install protobuf==3.5.0.post1
deactivate

log "Installing object detection models (tensorflow) in ${HOME}/.opt"
if [[ -d models ]]; then
  cd models
  git pull origin master || { log TENSORFLOW MODELS: UPDATE ERROR; exit 1; }
else
  git clone https://github.com/tensorflow/models || { log TENSORFLOW MODELS: GIT CLONE ERROR; exit 1; }
  cd models
fi

# for some reason, protoc cannot compile the most recent version of the files
git checkout c31b3c2
cd research
protoc object_detection/protos/*.proto --python_out=.

log "Installing pre-trained models (tensorflow) in ${HOME}/.opt/graph_def"
echo
GRAPH_DEF_DIR=$HOME/.opt/graph_def
mkdir -p ${GRAPH_DEF_DIR}
cd /tmp
curl -OL http://download.tensorflow.org/models/object_detection/faster_rcnn_resnet101_coco_11_06_2017.tar.gz
tar -xzf faster_rcnn_resnet101_coco_11_06_2017.tar.gz faster_rcnn_resnet101_coco_11_06_2017/frozen_inference_graph.pb
cp -a faster_rcnn_resnet101_coco_11_06_2017 ${GRAPH_DEF_DIR}
ln -sf ${GRAPH_DEF_DIR}/faster_rcnn_resnet101_coco_11_06_2017/frozen_inference_graph.pb ${GRAPH_DEF_DIR}/frozen_inference_graph.pb

cd $DIR

echo
log "Installation of WP2 Conjoint experiment: DONE"
log "Congrats!"


