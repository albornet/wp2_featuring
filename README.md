WP2 visual system featuring experiment
====================

This repository contains an experiment that runs all the models that are part of the NRP visual system developped in WP2 of SP10 and in collaboration with CDP4. This includes a framework for retina modelling, a deep neural network for saliency computation, a spiking neural network for early stage visual segmentation and a saccade mechanism. This repository will be updated when more models are integrated in the visual system.

Installation of the experiment
-----------

**Step 1 - Run the installation script**

* `bash install.sh` (run `bash uninstall.sh` to remove files from Models and Experiments).


**Step 2 - Run the experiment**

* Start the NRP and launch the experiment named "WP2 featuring experiment".
* Click the image viewer to see the output of the different models.
* Use the transfer function editor to change the saliency model that is being used.
